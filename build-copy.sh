#!/usr/bin/env bash

cp -R ./src/plugins/* ../app-base/node_modules/@catenon/app-common/src/plugins
cp -R ./src/plugins/* ../app-user/node_modules/@catenon/app-common/src/plugins
cp -R ./src/plugins/* ../app-client/node_modules/@catenon/app-common/src/plugins
cp -R ./src/plugins/* ../app-invoice/node_modules/@catenon/app-common/src/plugins
