const path = require('path')

module.exports = {
  configureWebpack: {
    output: {
      libraryExport: 'default'
    },
    resolve: {
      alias: {
        debug: 'debug/dist/debug.js',
        'catenonLib': path.resolve(__dirname, 'src/plugins')
      }
    }
    // devtool: 'source-map'
  },

  css: {
    sourceMap: true,
    loaderOptions: {
      sass: {
        prependData: `@import "@/styles/_variables";`
      }
    }
  },

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'assets/locales',
      enableInSFC: false
    }
  },

  assetsDir: 'assets',
  runtimeCompiler: true
}
