const { src, dest, watch, series } = require('gulp')
const path = require('path')
const yaml = require('gulp-yaml')
const yamlMerge = require('gulp-yaml-merge')

/**
 * Configuration
 */
const conf = {
  path: {
    src: 'src',
    plugins: 'src/plugins',
    assets: 'src/assets'
  }
}

const i18nFiles = [
  path.join(conf.path.plugins, 'i18n/*.en.yaml'),
  path.join(conf.path.src, 'i18n/*.en.yaml')
]

function i18n () {
  // English
  return src(i18nFiles)
    .pipe(yamlMerge('en.yaml'))
    .pipe(yaml({ space: 2 }))
    .pipe(dest(path.join(conf.path.assets, 'locales')))
}

function watching (cb) {
  watch(i18nFiles, i18n)
}

exports.i18n = i18n
exports.default = series(i18n, watching)
