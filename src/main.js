import '@/styles/app.scss'

import debug from 'debug'

import { CatenonAuth, CatenonAuthPlugin } from 'catenonLib/auth'

import Vue from 'vue'

import VueMask from 'v-mask'

import CatenonCore from 'catenonLib/core'
import CatenonPublicProfile from 'catenonLib/public-profile'
import CatenonSearch from 'catenonLib/search'

import App from './App.vue'
import router from './router'
import store from './store'
import i18n from './i18n'

/** @ignore */
const logger = debug('@catenon/main')

logger('Loading Common application...')

Vue.config.productionTip = false

const $preloader = document.getElementById('preloader')

const Auth = new CatenonAuth({ redirectAfterLogin: '/' })

// Catenon Plugins
Vue.use(CatenonAuthPlugin, { Auth, store, router })
Vue.use(CatenonCore, { store, router })
Vue.use(CatenonPublicProfile, { store, router })
Vue.use(CatenonSearch, { store, router })

Vue.use(VueMask)

const vue = new Vue({
  router,
  store,
  mounted () {
    $preloader.style.opacity = 0
    setTimeout(_ => {
      $preloader.style.display = 'none'
    }, 5000)
    logger('VUE Common application loaded!')
  },
  i18n,
  render: h => h(App)
})

logger('Retrieve Access Token...')
Auth.retrieveAccessToken()
  .then(_ => {
    store.dispatch('auth/currentUser', { currentUser: Auth.user })
    logger('Loading VUE Common application...')
    vue.$mount('#app')
  })
  .catch(error => {
    console.dir(error)
    if (error.response) {
      console.error('main:retrieveAccessToken error:', error.response.status, error.response.statusText)
      if (error.response.status === 401) {
        // redirect
        Auth.loginWithRedirect()
      }
    } else {
      console.error(error.toString())
      // vue.$mount('#app')
      // // window.location.href = '/errorpage'
      // console.dir(vue)
      // vue.$route.push('/errorpage')
    }
  })

export default vue
