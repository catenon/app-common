const CustomToast = {

  install (Vue) {
    Vue.config.errorHandler = function (err, vm, info) {
      // console.log('toast:errorHandler:', info)
      console.error(err)
      error(vm, err)
    }

    const re = new RegExp(/^([a-zA-Z]+[.]+[a-zA-Z])/)

    const toastOptions = {
      title: 'success',
      variant: 'success',
      toaster: 'b-toaster-top-center',
      autoHideDelay: 5000,
      solid: true
    }

    /**
     * @param {*} vm Instancia Vue
     * @param {*} body Puede ser una key de i18n o el texto
     * @param {*} title Puede ser una key de i18n o el texto
     */
    function success (vm, body, title = 'Success') {
      // console.log(re.test(body))
      if (re.test(body)) {
        body = vm.$t(body)
      }
      if (re.test(title)) {
        title = vm.$t(title)
      }
      // const toastOptions = {
      //   title,
      //   variant: 'success',
      //   toaster: 'b-toaster-top-center',
      //   autoHideDelay: 5000,
      //   solid: true
      // }
     toastOptions.title = title
     toastOptions.variant = 'success'
      vm.$bvToast.toast(`${body}`, toastOptions)
    }

    function info (vm, body, title = 'Information') {
      // console.log(re.test(body))
      if (re.test(body)) {
        body = vm.$t(body)
      }
      if (re.test(title)) {
        title = vm.$t(title)
      }
      toastOptions.title = title
      toastOptions.variant = 'info'
      // const toastOptions = {
      //   title,
      //   variant: 'info',
      //   toaster: 'b-toaster-top-center',
      //   autoHideDelay: 5000,
      //   solid: true
      // }
      vm.$bvToast.toast(`${body}`, toastOptions)
    }

    /**
     * @param {*} vm Instancia Vue
     * @param {*} body Puede ser una key de i18n o el texto
     * @param {*} title Puede ser una key de i18n o el texto
     */
    function error (vm, body, title = 'Error') {
      if (typeof body === 'object') {
        title = body.name
        body = body.message
      } else {
        if (re.test(body)) {
          body = vm.$t(body)
        }
        if (re.test(title)) {
          title = vm.$t(title)
        }
      }
      // const toastOptions = {
      //   title,
      //   variant: 'danger',
      //   toaster: 'b-toaster-top-center',
      //   autoHideDelay: 5000,
      //   solid: true
      // }
      toastOptions.title = title
      toastOptions.variant = 'danger'
      vm.$bvToast.toast(`${body}`, toastOptions)
    }

    Vue.prototype.$toast = {
      success,
      error,
      info
    }
  }

}

export default CustomToast
