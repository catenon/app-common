import vue from '@/main.js'

const errorHandlerAPI = function (error, $this, text, title) {
  console.dir(error)
  console.log(!!error.results, !!error.results.length, error.http_code === 400)
  if (!!error.results && !!error.results.length && error.http_code === 400) {
    const err = error.results[0]
    $this.$toast.error($this, err.error_message, title)
  } else {
    $this.$toast.error($this, error)
    console.error(error)
  }
  $this.$loading(false)
}

const onError = function (error) {
  const params = {}
  if (error.response) {
    params.code = error.response.status
    params.message = error.response.statusText
    params.text = error.response.statusText
  }
  vue.$router.push({ name: 'errorPage', params })
  console.dir(typeof error)
  console.dir(error)
  console.error(error)
  vue.$loading(false)
}

export {
  onError,
  errorHandlerAPI
}

export default {
  onError,
  errorHandlerAPI
}
