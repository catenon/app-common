/**
 * @deprecated
 */
import moment from 'moment'
import { isEmpty } from 'lodash'

/**
 * @param form
 * @param field
 * @returns {*}
   */
function hasFormError (form, field) {
  if (field) {
    return field.$invalid && (field.$dirty || form.$submitted)
  }
  return false
}

/**
 * Valida los campos de los formularios
 * @param form
 * @param field
 * @returns {*}
   */
function validateInput (form, name) {
  var input = form[name]
  return (input.$dirty || form.$submitted) && input.$invalid
}

function validateEmail (input) {
  var currentEmail = input.$viewValue

  // eslint-disable-next-line no-useless-escape
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(currentEmail)) {
    return false
  } else {
    input.$error = { email: true }
    return true
  }
}

function parseDateFromYYYYMMMM (year, month) {
  try {
    if (isEmpty(year) || isEmpty(month)) {
      return false
    }
    var m = moment([year, month], 'YYYY MMMM')
    return (m.isValid()) ? m.format('YYYY-MM-DDT01:01:01') : false
  } catch (e) {
    return false
  }
}

function parseDateToYYYYMMMM (date) {
  try {
    if (isEmpty(date)) {
      return false
    }
    var m = moment(date, moment.ISO_8601)
    return (m.isValid()) ? { year: m.format('YYYY'), month: m.format('MMMM') } : false
  } catch (e) {
    return false
  }
}

function parseDateFromYYYYMMDD (year, month, day) {
  try {
    if (isEmpty(year) || isEmpty(month) || isEmpty(day)) {
      return false
    }
    if (!_isNumber(year) || !_isNumber(month) || !_isNumber(day)) {
      return false
    }
    var m = moment([year, month, day], 'YYYY MM DD')
    return (m.isValid()) ? m.format('YYYY-MM-DDT01:01:01') : false
  } catch (e) {
    return false
  }
}

function _isNumber (n) {
  var pat = /^\d+$/
  return pat.test(n)
}

function parseDateToYYYYMMDD (date) {
  try {
    if (isEmpty(date)) {
      return false
    }
    var m = moment(date, moment.ISO_8601)
    return (m.isValid()) ? { year: m.format('YYYY'), month: m.format('MM'), day: m.format('DD') } : false
  } catch (e) {
    return false
  }
}

function parseShortDate (date) {
  return moment(date).format('DD/MM/YYYY')
}

/**
 * Crea un nuevo objeto a partir de otr
 * @param {*} source
 * @param {*} props
 */
function pickObj (source, props) {
  const obj = {}
  if (!source) {
    source = {}
  }
  props.forEach(function (prop) {
    obj[prop] = source[prop]
  })
  return obj
}

/**
 * Crea un nuevo objeto a partir de oto con copia profunda
 * @param {*} source
 * @param {*} props
 */
function deepClone (obj, hash = new WeakMap()) {
  if (Object(obj) !== obj) return obj // primitives
  if (obj instanceof Set) return new Set(obj) // See note about this!
  if (hash.has(obj)) return hash.get(obj) // cyclic reference
  const result = obj instanceof Date ? new Date(obj)
    : obj instanceof RegExp ? new RegExp(obj.source, obj.flags)
      : obj.constructor ? new obj.constructor()
        : Object.create(null)
  hash.set(obj, result)
  if (obj instanceof Map) {
    Array.from(obj, ([key, val]) => result.set(key, deepClone(val, hash)))
  }
  return Object.assign(result, ...Object.keys(obj).map(key => ({ [key]: deepClone(obj[key], hash) })))
}

/**
 * Retorna el objeto de un array
 * @param {Array} source
 * @param {Object} key
 */
function findObj (source, key) {
  let obj = null
  if (source) {
    const keys = Object.keys(key)
    if (keys && keys.length) {
      obj = source.find(item => {
        const prop = keys[0]
        return item[prop] === key[prop]
      })
    }
  }
  return obj
}

function parseItemDates (item) {
  let copy = deepClone(item)
  if (!item.start) copy.start = getYYYYMMFromDate(copy.startDate)
  if (!item.end) copy.end = getYYYYMMFromDate(copy.endDate)
  return copy
}

function parseDate (value) {
  if (value) {
    const date = new Date(value)
    return date.toLocaleDateString(['en-US'], { month: 'long', year: 'numeric' })
  }
}

function parseHitDate (value) {
  if (value) {
    const date = new Date(value)
    return date.toLocaleDateString(['en-US'], { month: 'short', day: 'numeric', year: 'numeric' })
  }
}

function months () {
  return [
    'Select...',
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'Dicember'
  ]
}

function getYYYYMMFromDate (value) {
  if (value) {
    const date = new Date(value)
    return { year: date.getFullYear(), month: date.getMonth() + 1 }
  } else {
    return { year: '', month: 0 }
  }
}

function getDateFromYYYYMM (value) {
  if (value.year !== '' && value.month !== 0) {
    const date = new Date()
    date.setFullYear(value.year)
    date.setMonth(value.month - 1)
    return date.toISOString().replace('Z', '')
  } else {
    return undefined
  }
}

// TODO make test
/**
 * Valida las fechas
 * @param form
 * @param name
 * @param scope
   * @returns {*}
   */
function getValidDate (form, name, scope) {
  var field = form[name]
  if (!isEmpty(scope[name].year) || !isEmpty(scope[name].month) || !isEmpty(scope[name].day)) {
    field.$setDirty()
    var date = parseDateFromYYYYMMDD(scope[name].year, scope[name].month, scope[name].day)
    if (date) {
      field.$setValidity('date', true)
      return date
    } else {
      field.$setValidity('date', false)
      return ''
    }
  } else {
    field.$setValidity('date', true)
    return ''
  }
}

function getErrorMessage (error) {
  if (error.data) {
    if (!isEmpty(error.data.results)) {
      return error.data.results[0].error_message
    }
    return error.data.message
  }
  return (error.statusText) ? error.statusText : 'Catenon, we have a problem!'
}

function openWindow (url, target) {
  if (url.indexOf('http') >= 0) {
    window.open(url, target)
  } else {
    location.path(url)
  }
}

function parseMimeType (mime) {
  let type = mime
  const types = ['text', 'image', 'audio', 'video', 'application', 'document', 'data', 'json']
  types.forEach(_type => {
    if (mime.includes(_type)) {
      type = _type.charAt(0).toUpperCase() + _type.slice(1)
    }
  })
  return type
}

function getFormattedDate (date) {
  if (date) {
    return moment(date, 'YYYY/MM/DD').format('YYYY-MM-DDT01:01:01.000')
  }
  return null
}

function isValidField (field) {
  if (field && field.$dirty) {
    return !field.$error
  }
  return null
}

function getIdFromUrl (url) {
  return url.substr(url.lastIndexOf('/') + 1)
}

function getLinkFromUrl (url, key) {
  return url.substr(url.indexOf(key))
}

function isBeforeLessThanAfter (before, after) {
  console.log(before, after)
  if (!before && after) {
    return false
  }
  if (before && after) {
    return moment(before).isBefore(moment(after))
  }
  return true
}

export {
  hasFormError,
  validateInput,
  validateEmail,
  parseDateFromYYYYMMMM,
  parseDateToYYYYMMMM,
  parseDateFromYYYYMMDD,
  parseDateToYYYYMMDD,
  getValidDate,
  getErrorMessage,
  openWindow,
  parseMimeType,
  findObj,
  pickObj,
  parseDate,
  deepClone,
  months,
  getYYYYMMFromDate,
  getDateFromYYYYMM,
  parseItemDates,
  parseHitDate,
  getFormattedDate,
  isValidField,
  getIdFromUrl,
  getLinkFromUrl,
  isBeforeLessThanAfter,
  parseShortDate
}

export default {
  hasFormError,
  validateInput,
  validateEmail,
  getLinkFromUrl,
  parseDateFromYYYYMMMM,
  parseDateToYYYYMMMM,
  parseDateFromYYYYMMDD,
  parseDateToYYYYMMDD,
  getValidDate,
  getErrorMessage,
  openWindow,
  parseMimeType,
  findObj,
  pickObj,
  parseDate,
  deepClone,
  months,
  getYYYYMMFromDate,
  getDateFromYYYYMM,
  parseItemDates,
  parseHitDate,
  getFormattedDate,
  isValidField,
  getIdFromUrl,
  isBeforeLessThanAfter,
  parseShortDate
}
