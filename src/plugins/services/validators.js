import moment from 'moment'
import { helpers } from 'vuelidate/lib/validators'

function dateLessThan (afterDateField) {
  return helpers.withParams({ type: 'dateLessThan', afterDateField }, function (beforeDate, vm) {
    const afterDate = helpers.ref(afterDateField, this, vm)
    if (beforeDate === '' || afterDate === '') {
      return true
    }
    return moment(afterDate, 'YYYY/MM/DD').isBefore(moment(beforeDate, 'YYYY/MM/DD'))
  })
}

export { dateLessThan }

export default { dateLessThan }
