const FormMixins = {
  methods: {
    isValidField (field) {
      if (field && field.$dirty) {
        return !field.$error
      }
      return null
    }
  }
}

const LoadingMixins = {
  data: function () {
    return {
      isLoading: true,
      loadingError: null

    }
  },
  computed: {
    hasLoadingError () {
      return !!this.loadingError
    }
  }
}

export { FormMixins, LoadingMixins }
export default { FormMixins, LoadingMixins }
