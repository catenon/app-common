import * as mutations from './publicProfile.mutations'
import * as actions from './publicProfile.actions'
import * as getters from './publicProfile.getters'

export default {
  namespaced: true,
  state () {
    return {
      publicProfile: {
        id: 0
      },
      profilePhotoUrl: require('@/assets/images/avatar.png'),
      socialMediaProfiles: {},
      educations: [],
      languageSkills: [],
      professionalExperiences: [],
      skills: [],
      experienceEdit: {},
      educationEdit: {},
      sectors: [],
      areas: []
    }
  },
  getters,
  mutations,
  actions
}
