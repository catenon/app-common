import { find } from 'lodash'
import utils from 'catenonLib/services/utils'
import { PublicProfiles } from 'catenonLib/resources'
// import utils from 'catenonLib/services/helpers'

export async function loadById ({ commit, state }, id) {
  const { data: allSectors } = await PublicProfiles.loadSectors()
  const { data: allAreas } = await PublicProfiles.loadFunctionalAreas()
  commit('SET_EXPERIENCE_RESOURCES', { sectors: allSectors._embedded.sectors, areas: allAreas._embedded.functionalAreas })

  const { data: publicProfile } = await PublicProfiles.getPublicProfile(id)
  if (publicProfile) {
    const photoUrl = (publicProfile._links.profilePhotoUrl) ? publicProfile._links.profilePhotoUrl.href : require('@/assets/images/avatar.png')
    commit('SET_PROFILE_PHOTO_URL', photoUrl)
    commit('SET_PUBLIC_PROFILE', publicProfile)
  }
  const { data: socialMediaProfile } = await PublicProfiles.loadSocialMediaProfiles(id)
  if (socialMediaProfile) {
    let socialMediaProfiles = {
      facebook: {
        serviceName: 'Facebook',
        contactUrl: null,
        description: null,
        profileUrl: null,
        username: null,
        socialMedia: '/4'
      },
      twitter: {
        serviceName: 'Twitter',
        contactUrl: null,
        description: null,
        profileUrl: null,
        username: null,
        socialMedia: '/1'
      },
      linkedin: {
        serviceName: 'LinkedIn',
        contactUrl: null,
        description: null,
        profileUrl: null,
        username: null,
        socialMedia: '/3'
      },
      skype: {
        serviceName: 'Skype',
        contactUrl: null,
        description: null,
        profileUrl: null,
        username: null,
        socialMedia: '/2'
      }
    }
    let profiles = socialMediaProfile._embedded.socialMediaProfiles
    profiles.forEach(function (media) {
      if (media.serviceName === 'Facebook') {
        socialMediaProfiles.facebook.profileUrl = media.profileUrl
        socialMediaProfiles.facebook.id = media.id
      } else if (media.serviceName === 'Twitter') {
        socialMediaProfiles.twitter.username = media.username
        socialMediaProfiles.twitter.id = media.id
      } else if (media.serviceName === 'LinkedIn') {
        socialMediaProfiles.linkedin.profileUrl = media.profileUrl
        socialMediaProfiles.linkedin.id = media.id
      } else if (media.serviceName === 'Skype') {
        socialMediaProfiles.skype.username = media.username
        socialMediaProfiles.skype.id = media.id
      }
    })
    commit('SET_PUBLIC_SOCIAL_MEDIA_PROFILES', socialMediaProfiles)
  }

  const { data: profileEducation } = await PublicProfiles.loadProfileEducation(id)
  if (profileEducation) {
    let parsed = []
    profileEducation._embedded.educations.forEach(function (edu) {
      parsed.push(utils.parseItemDates(edu))
    })
    commit('SET_PROFILE_EDUCATION', parsed)
  }

  const { data: professionalExperiences } = await PublicProfiles.loadProfessionalExperiences(id)
  if (professionalExperiences) {
    let experiences = professionalExperiences._embedded.professionalExperiences
    let parsed = []
    const sectors = this.getters['publicProfile/getSectors']
    const areas = this.getters['publicProfile/getAreas']
    experiences.forEach(function (exp) {
      let experience = utils.parseItemDates(exp)
      // experience.sector = ''
      if (experience.sector) {
        var sector = find(sectors, { keyCode: experience.sector })
        if (sector) {
          experience.sector = sector._links.self.href
          experience.sectorName = sector.name
        }
      }
      if (experience.area) {
        // experience.area = ''
        var area = find(areas, { keyCode: experience.area })
        if (area) {
          experience.area = area._links.self.href
          experience.areaName = area.name
        }
      }
      parsed.push(experience)
    })
    commit('SET_PROFESSIONAL_EXPERIENCES', parsed)
  }
  const { data: languageSkills } = await PublicProfiles.loadLanguageSkills(id)
  commit('SET_LANGUAGE_SKILLS', languageSkills._embedded.languageSkills)
}

export async function load ({ commit }, { publicProfile, socialMediaProfile, professionalExperiences, profileEducation }) {
  // modifica el objeto y se se debe comunicar con el resto de componentes del public profile
  if (publicProfile) {
    const photoUrl = (publicProfile._links.profilePhotoUrl) ? publicProfile._links.profilePhotoUrl.href : require('@/assets/images/avatar.png')
    commit('publicProfile/SET_PROFILE_PHOTO_URL', photoUrl, { root: true })
    commit('publicProfile/SET_PUBLIC_PROFILE', publicProfile, { root: true })
  }
  if (socialMediaProfile) {
    let socialMediaProfiles = {
      facebook: {
        serviceName: 'Facebook',
        contactUrl: null,
        description: null,
        profileUrl: null,
        username: null,
        socialMedia: '/4'
      },
      twitter: {
        serviceName: 'Twitter',
        contactUrl: null,
        description: null,
        profileUrl: null,
        username: null,
        socialMedia: '/1'
      },
      linkedin: {
        serviceName: 'LinkedIn',
        contactUrl: null,
        description: null,
        profileUrl: null,
        username: null,
        socialMedia: '/3'
      },
      skype: {
        serviceName: 'Skype',
        contactUrl: null,
        description: null,
        profileUrl: null,
        username: null,
        socialMedia: '/2'
      }
    }
    let profiles = socialMediaProfile._embedded.socialMediaProfiles
    profiles.forEach(function (media) {
      if (media.serviceName === 'Facebook') {
        socialMediaProfiles.facebook.profileUrl = media.profileUrl
        socialMediaProfiles.facebook.id = media.id
      } else if (media.serviceName === 'Twitter') {
        socialMediaProfiles.twitter.username = media.username
        socialMediaProfiles.twitter.id = media.id
      } else if (media.serviceName === 'LinkedIn') {
        socialMediaProfiles.linkedin.profileUrl = media.profileUrl
        socialMediaProfiles.linkedin.id = media.id
      } else if (media.serviceName === 'Skype') {
        socialMediaProfiles.skype.username = media.username
        socialMediaProfiles.skype.id = media.id
      }
    })
    commit('publicProfile/SET_PUBLIC_SOCIAL_MEDIA_PROFILES', socialMediaProfiles, { root: true })
  }
  if (professionalExperiences) {
    let experiences = professionalExperiences._embedded.professionalExperiences
    let parsed = []
    const sectors = this.getters['publicProfile/getSectors']
    const areas = this.getters['publicProfile/getAreas']
    experiences.forEach(function (exp) {
      let experience = utils.parseItemDates(exp)
      if (experience.sector) {
        var sector = find(sectors, { keyCode: experience.sector })
        if (sector) {
          experience.sector = sector._links.self.href
          experience.sectorView = sector.name
        }
      }
      if (experience.area) {
        var area = find(areas, { keyCode: experience.area })
        experience.area = area._links.self.href
        experience.areaName = area.name
      }
      parsed.push(experience)
    })
    commit('publicProfile/SET_PROFESSIONAL_EXPERIENCES', parsed, { root: true })
  }
  if (profileEducation) {
    let parsed = []
    profileEducation._embedded.educations.forEach(function (edu) {
      parsed.push(utils.parseItemDates(edu))
    })
    commit('publicProfile/SET_PROFILE_EDUCATION', parsed, { root: true })
  }
}

// export async function loadResources ({ commit }, resources) {
//   resources.sectors = resources.sectors._embedded.sectors
//   resources.areas = resources.areas._embedded.functionalAreas
//   commit('publicProfile/SET_EXPERIENCE_RESOURCES', resources, { root: true })
// }

export async function loadLanguageSkills ({ commit }, data) {
  // TODO: mejorar esto usando store
  // const languages = JSON.parse(localStorage.getItem('languages'))
  // const levels = JSON.parse(localStorage.getItem('languageSkillLevels'))
  // const languageSkills = data.languageSkills._embedded.languageSkills.map(lang => {
  //   const language = utils.findObj(languages, { link: lang._links.language.href })
  //   const level = utils.findObj(levels, { link: lang._links.level.href })
  //   return {
  //     id: lang.id,
  //     language: lang._links.language.href,
  //     level: lang._links.level.href,
  //     description: lang.description,
  //     _name: language ? language.name : '',
  //     _level: level ? level.name : ''
  //   }
  // })
  // if (languageSkills) {
  commit('SET_LANGUAGE_SKILLS', data.languageSkills._embedded.languageSkills)
  // }
}

export async function updatePublicProfile ({ commit }, { id, data }) {
  const { data: publicProfile } = await PublicProfiles.update(id, data)
  const photoUrl = (publicProfile._links.profilePhotoUrl) ? publicProfile._links.profilePhotoUrl.href : require('@/assets/images/avatar.png')
  commit('SET_PROFILE_PHOTO_URL', photoUrl)
  commit('SET_PUBLIC_PROFILE', publicProfile)
}

// TODO: Esto se tiene que refactorizar
export async function resetPublicProfile ({ state }) {
  state.publicProfile = { id: 0 }
  state.profilePhotoUrl = require('@/assets/images/avatar.png')
  state.socialMediaProfiles = {}
  state.educations = []
  state.languageSkills = []
  state.professionalExperiences = []
  state.skills = []
  state.experienceEdit = {}
  state.educationEdit = {}
  // state.sectors = []
  // state.areas = []
}
