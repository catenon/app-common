export const getPublicProfile = state => state.publicProfile

export const getProfilePhotoUrl = state => state.profilePhotoUrl

export const getSocialMediaProfiles = state => state.socialMediaProfiles

export const getLanguageSkills = state => state.languageSkills

export const isLanguageSkillsEditMode = state => state.languageSkillsEditMode

export const getProfessionalExperiences = state => state.professionalExperiences

export const getProfileEducation = state => state.educations

// esto deberia de ir en otro store?
export const getSectors = state => state.sectors
export const getAreas = state => state.areas
