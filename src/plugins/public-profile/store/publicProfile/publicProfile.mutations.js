import Vue from 'vue'
import { find } from 'lodash'
import utils from 'catenonLib/services/utils'

const extendExperience = function (experience, areas, sectors) {
  if (experience.sector) {
    // experience.sector = ''
    // experience.sectorName = ''
    const sector = find(sectors, { keyCode: experience.sector })
    if (sector) {
      experience.sector = sector._links.self.href
      experience.sectorName = sector.name
    }
  }
  if (experience.area) {
    // experience.area = ''
    // experience.areaName = ''
    const area = find(areas, { keyCode: experience.area })
    experience.area = area._links.self.href
    experience.areaName = area.name
  }
  return experience
}

export function SET_PUBLIC_PROFILE (state, publicProfile) {
  state.publicProfile = publicProfile
}
export function SET_PROFILE_PHOTO_URL (state, url) {
  state.profilePhotoUrl = url
}
export function SET_PUBLIC_SOCIAL_MEDIA_PROFILES (state, socialMediaProfiles) {
  state.socialMediaProfiles = socialMediaProfiles
}
export function SET_PROFESSIONAL_EXPERIENCES (state, professionalExperiences) {
  state.professionalExperiences = professionalExperiences
}
export function SET_EXPERIENCE_RESOURCES (state, { sectors, areas }) {
  state.areas = areas
  state.sectors = sectors
}
export function SET_PROFILE_EDUCATION (state, profileEducation) {
  state.educations = profileEducation
}
export function SET_LANGUAGE_SKILLS (state, languageSkills) {
  const languages = JSON.parse(localStorage.getItem('languages'))
  const levels = JSON.parse(localStorage.getItem('languageSkillLevels'))
  const mapLanguageSkills = languageSkills.map(lang => {
    const language = find(languages, { link: lang._links.language.href })
    const level = find(levels, { link: lang._links.level.href })
    return {
      id: lang.id,
      language: lang._links.language.href,
      level: lang._links.level.href,
      description: lang.description,
      _name: language ? language.name : '',
      _level: level ? level.name : ''
    }
  })
  state.languageSkills = mapLanguageSkills
}

export function ADD_PROFESSIONAL_EXPERIENCES (state, experience) {
  const sectors = this.getters['publicProfile/getSectors']
  const areas = this.getters['publicProfile/getAreas']
  experience = utils.parseItemDates(experience)
  experience = extendExperience(experience, areas, sectors)
  state.professionalExperiences.push(experience)
}

export function UPDATE_PROFESSIONAL_EXPERIENCES (state, { index, experience }) {
  const sectors = this.getters['publicProfile/getSectors']
  const areas = this.getters['publicProfile/getAreas']
  experience = utils.parseItemDates(experience)
  experience = extendExperience(experience, areas, sectors)
  Vue.set(state.professionalExperiences, index, experience)
  // state.professionalExperiences = experience
}

// export async function addProfessionalExperience ({ commit, state }, data) {
//   state.professionalExperiences.push(data)
// }

// export async function updateProfessionalExperience ({ commit, state }, idex, data) {
//   Vue.set()
// }
