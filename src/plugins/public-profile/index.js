import debug from 'debug'
import routes from './publicProfile.route'
import publicProfileStore from './store/publicProfile'

import Summary from './components/summary/Summary.vue'
import Details from './components/details/Details.vue'
import Skills from './components/skills/Skills.vue'
import Experience from './components/experience/Experience.vue'
import Education from './components/education/Education.vue'
import Languages from './components/languages/Languages.vue'

const logger = debug('@catenon/publicProfile')

const CatenonPublicProfile = {
  install (Vue, {
    router,
    store
  }) {
    logger('Installing CatenonPublicProfile plugin...')

    router.addRoutes(routes)
    store.registerModule('publicProfile', publicProfileStore)

    Vue.component('cw-public-profile-summary', Summary)
    Vue.component('cw-public-profile-details', Details)
    Vue.component('cw-public-profile-experience', Experience)
    Vue.component('cw-public-profile-education', Education)
    Vue.component('cw-public-profile-languages', Languages)
    Vue.component('cw-public-profile-skills', Skills)
  }
}

export { CatenonPublicProfile }

export default CatenonPublicProfile
