import Me from './views/Me'

export default [
  {
    path: '/me',
    component: Me
  }
]
