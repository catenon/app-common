import VueInstance from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// import VeeValidate from 'vee-validate'
import moment from 'moment'

import routes from './core.route'
import ui from './store/ui'
// import users from './store/users' // TODO: revisar esto
// import i18n, { loadLanguageAsync } from './config/i18n'
import Vuelidate from 'vuelidate'

import utils from '../services/utils'

// import Feather from './components/Feather.vue'
import Navbar from './components/navbar/Navbar.vue'
import Sidebar from './components/sidebar/Sidebar.vue'
import Link from './components/sidebar/Link.vue'
// import CatenonAuth from '../auth'
import Avatar from './components/avatar/Avatar'
import Panel from './components/panel/Panel.vue'
import InputLabel from './components/inputs/InputLabel.vue'
import InputForm from './components/inputs/InputForm.vue'
import HitItemContainer from './components/item-containers/HitItemContainer.vue'
import BasicItemContainer from './components/item-containers/BasicItemContainer.vue'
import Tag from './components/Tag.vue'
import Typeahead from './components/typeahead/VueBootstrapTypeahead.vue'

import Loading from 'catenonLib/loading'
import CustomToast from 'catenonLib/services/toast.js'

import ErrorPage from './views/ErrorPage.vue'
import PageNotFound from './views/PageNotFound.vue'
import PageForbidden from './views/PageForbidden.vue'

import BrowserLoader from './components/content-loaders/BrowserLoader.vue'
import ProfileLoader from './components/content-loaders/ProfileLoader.vue'
import PanelLoader from './components/content-loaders/PanelLoader.vue'
import ResultsLoader from './components/content-loaders/ResultsLoader.vue'
import ItemResultLoader from './components/content-loaders/ItemResultLoader.vue'

import ReadMore from './directives/read-more'
import BlueHeader from './components/headers/BlueHeader.vue'
import PageHeader from './components/headers/PageHeader.vue'

const CatenonCore = {
  install (Vue, { router, store }) {
    console.log('Installing CatenonCore plugin...')

    // use bootstrap-vue
    Vue.use(BootstrapVue)
    Vue.use(IconsPlugin)

    // // use vee-validate
    // Vue.use(VeeValidate, {
    //   // This is the default
    //   inject: true,
    //   // Important to name this something other than 'fields'
    //   fieldsBagName: 'veeFields',
    //   // This is not required but avoids possible naming conflicts
    //   errorBagName: 'veeErrors'
    // })

    Vue.use(Vuelidate)

    Vue.use(CustomToast)
    Vue.use(Loading)

    // set modules
    Vue.prototype.$moment = moment
    Vue.prototype.$util = utils
    Vue.prototype.$eventBus = new VueInstance()

    Vue.filter('shortDate', utils.parseShortDate)
    Vue.filter('date', utils.parseDate)
    Vue.filter('hitDate', utils.parseHitDate)

    router.addRoutes(routes)

    store.registerModule('ui', ui)
    // store.registerModule('users', users)

    Vue.component('cw-avatar', Avatar)
    Vue.component('cw-navbar', Navbar)
    Vue.component('cw-sidebar', Sidebar)
    Vue.component('cw-link', Link)
    Vue.component('cw-input-label', InputLabel)
    Vue.component('cw-input-form', InputForm)
    Vue.component('cw-hit-item-container', HitItemContainer)
    Vue.component('cw-basic-item-container', BasicItemContainer)
    Vue.component('cw-panel', Panel)
    Vue.component('cw-tag', Tag)
    Vue.component('cw-blue-header', BlueHeader)
    Vue.component('cw-page-header', PageHeader)
    // Vue.component('cw-fi', Feather)

    Vue.component('cw-error-page', ErrorPage)
    Vue.component('cw-page-not-found', PageNotFound)
    Vue.component('cw-page-forbidden', PageForbidden)

    Vue.component('cw-panel-loader', PanelLoader)
    Vue.component('cw-browser-loader', BrowserLoader)
    Vue.component('cw-profile-loader', ProfileLoader)
    Vue.component('cw-results-loader', ResultsLoader)
    Vue.component('cw-item-result-loader', ItemResultLoader)
    Vue.component('cw-vue-bootstrap-typeahead', Typeahead)

    Vue.directive('read-more', ReadMore)

    router.afterEach((to, from) => {
      if (store && store.getters['ui/isSidebarOpened']) {
        store.commit('ui/toggleSidebar')
      }
    })
  }
}

export { CatenonCore }

export default CatenonCore
