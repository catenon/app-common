import ErrorPage from './views/ErrorPage'
import PageNotFound from './views/PageNotFound'
import PageForbidden from './views/PageForbidden'

export default [
  {
    path: '/error',
    name: 'errorPage',
    component: ErrorPage,
    props: true
  },
  {
    path: '/forbidden',
    component: PageForbidden
  },
  {
    path: '*',
    component: PageNotFound
  }
]
