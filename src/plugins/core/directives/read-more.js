import { truncate } from 'lodash'
export default {
  update (el, binding) {
    if (binding.value && binding.value.length > binding.arg) {
      const longText = binding.value
      const shortText = truncate(longText, { length: binding.arg })

      el.textContent = shortText

      const readMore = document.createElement('a')
      readMore.style.fontStyle = 'italic'
      readMore.href = '#'
      readMore.text = 'read more'

      const readLess = document.createElement('a')
      readLess.style.fontStyle = 'italic'
      readLess.href = '#'
      readLess.text = 'read less'

      el.append(' ', readMore)

      readMore.addEventListener('click', function () {
        el.textContent = longText
        el.append(' ', readLess)
      })

      readLess.addEventListener('click', function () {
        el.textContent = shortText
        el.append(' ', readMore)
      })
    } else {
      el.textContent = binding.value || '-'
    }
  }
}
