// export const getTitle = state => state.title

export const getShowEditIcon = state => state.showEditIcon

export const getCreateModeState = state => state.isCreateMode

export const getUpdateModeState = state => state.isUpdateMode

export const getFacets = state => state.facets

export const getUsers = state => state.users

export const getTotal = state => state.total

export const getIsFiltered = state => state.isFiltered
