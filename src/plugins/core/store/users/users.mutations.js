/* export const setUser = (state, value) => {
  state.currentUser.name = value
} */

/* export const setUsers = (state, users) => {
  state.users = users
} */

export function setUsers (state, users) {
  state.users = users
}

export function setCreateMode (state) {
  state.isCreateMode = !state.isCreateMode
}

export function setUpdateMode (state) {
  state.isUpdateMode = !state.isUpdateMode
}

export function setIsFiltered (state) {
  state.isFiltered = !state.isFiltered
}

export function setFacets (state, facets) {
  state.facets = facets
}

export function setTotal (state, total) {
  state.total = total
}
