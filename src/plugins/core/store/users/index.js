import * as mutations from './users.mutations'
import * as actions from './users.actions'
import * as getters from './users.getters'

export default {
  namespaced: true,
  state () {
    return {
      users: [],
      facets: [],
      total: 0,
      isCreateMode: false,
      isUpdateMode: false,
      orderByName: false,
      orderByDate: false,
      orderByPosition: false,
      order: null,
      direction: null,
      isFiltered: false
    }
  },
  getters,
  mutations,
  actions
}
