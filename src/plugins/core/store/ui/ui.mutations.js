export const SET_LOADING = (state, value) => {
  state.loading = value
}

export const SET_TITLE = (state, value) => {
  state.title = value
}

// export const setUser = (state, value) => {
//   state.currentUser.name = value
// }

// export const setOpenApp = (state, value) => {
//   state.app.NAME = value
// }

export const toggleSidebar = (state) => {
  state.isSidebarOpened = !state.isSidebarOpened
}

// export const closeSideBar = (state) => {
//   state.isSidebarClosed = false
// }

export const setToggleMenu = (state) => {
  state.toggleMenu = !state.toggleMenu
}

export const setCloseMenu = (state) => {
  state.toggleMenu = false
}

export const setClosePopover = (state) => {
  state.popoverApp = false
}

export const ENABLE_SIDEBAR_MINI = (state) => {
  state.isSidebarMini = true
}
