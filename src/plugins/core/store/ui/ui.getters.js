
export const isLoading = state => state.loading

// export const getUser = state => state.currentUser.name

// export const getOpenApp = state => state.app.NAME

// export const getSideBar = state => state.isSidebarClosed

export const isSidebarOpened = state => state.isSidebarOpened

// export const getOptionName = state => state.optionName

export const getToggleMenu = state => state.toggleMenu

export const isSidebarMini = state => state.isSidebarMini
