import * as mutations from './ui.mutations'
import * as actions from './ui.actions'
import * as getters from './ui.getters'

export default {
  namespaced: true,
  state () {
    return {
      loading: true,
      isSidebarOpened: false,
      // isSidebarClosed: false,
      toggleMenu: false,
      popoverApp: false,
      isSidebarMini: false
      // currentUser: {
      //   name: 'Administrator'
      // },
      // app: {
      //   NAME: 'USER APP'
      // },
      // optionName: 'Browse'
    }
  },
  getters,
  mutations,
  actions
}
