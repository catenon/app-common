import Facets from './components/facets/Facets.vue'
import FacetsMobile from './components/facets/FacetsMobile'
import ResultHit from './components/hits/ResultHit.vue'
// import OrderByItem from './components/orderBy/OrderByItem.vue'
// import OrderByList from './components/orderBy/OrderByList.vue'
import OrderBy from './components/orderBy/OrderBy.vue'
import Pagination from './components/pagination/Pagination.vue'
import PaginationAdvanced from './components/pagination/PaginationAdvanced.vue'
import Quicksearch from './components/quicksearch/Quicksearch.vue'
import QuicksearchAdvanced from './components/quicksearchAdvanced/QuicksearchAdvanced.vue'
import AdvancedFilters from './components/searchFilters/AdvancedFilters.vue'
import AdvancedFilterFields from './components/searchFilters/fields/AdvancedFilterFields.vue'
import DateFilter from './components/searchFilters/fields/date/DateFilter.vue'
import StringFilter from './components/searchFilters/fields/string/StringFilter.vue'
import MultipleFilter from './components/searchFilters/fields/multiple/MultipleFilter.vue'
import RangeFilter from './components/searchFilters/fields/range/RangeFilter.vue'
import NumberFilter from './components/searchFilters/fields/number/NumberFilter.vue'
import BooleanFilter from './components/searchFilters/fields/boolean/BooleanFilter.vue'
import IdFilter from './components/searchFilters/fields/id/IdFilter.vue'
import IdFilterResults from './components/searchFilters/fields/id/IdFilterResults.vue'
import SearchLetters from './components/searchletters/SearchLetters.vue'
import searchStore from './store/search'

const CatenonCore = {
  install (Vue, { store }) {
    console.log('Installing CatenonSearch plugin...')

    // reemplaza el search por defecto (user) por el definido en cada app
    if (!store.state.search) {
      store.registerModule('search', searchStore)
    }

    Vue.component('cw-result-hit', ResultHit)

    Vue.component('cw-quicksearch', Quicksearch)
    Vue.component('cw-quicksearch-advanced', QuicksearchAdvanced)
    Vue.component('cw-search-pagination', Pagination)
    Vue.component('cw-search-advanced-pagination', PaginationAdvanced)
    // Vue.component('cw-search-order-by', OrderByList)
    Vue.component('cw-search-order-by', OrderBy)
    Vue.component('cw-search-letters', SearchLetters)
    Vue.component('cw-search-filters', Facets)
    Vue.component('cw-search-filters-mobile', FacetsMobile)
    Vue.component('cw-search-advanced-filters', AdvancedFilters)
    Vue.component('cw-search-advanced-filter-field', AdvancedFilterFields)
    Vue.component('cw-search-advanced-filter-field-date', DateFilter)
    Vue.component('cw-search-advanced-filter-field-string', StringFilter)
    Vue.component('cw-search-advanced-filter-field-multiple', MultipleFilter)
    Vue.component('cw-search-advanced-filter-field-range', RangeFilter)
    Vue.component('cw-search-advanced-filter-field-number', NumberFilter)
    Vue.component('cw-search-advanced-filter-field-bool', BooleanFilter)
    Vue.component('cw-search-advanced-filter-field-id', IdFilter)
    Vue.component('cw-search-advanced-filter-field-id-results', IdFilterResults)
    // Vue.component('cw-search-order-by-item', OrderByItem)
  }
}

export { CatenonCore }

export default CatenonCore
