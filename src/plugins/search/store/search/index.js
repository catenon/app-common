import * as mutations from './search.mutations'
import * as actions from './search.actions'
import * as getters from './search.getters'

const state = function () {
  return {
    filters: {
      type: 'professional',
      query: '',
      fieldsFilter: [
        { name: 'validationStatus', values: ['Activated'] },
        { name: 'active', values: [true] }
      ],
      pageable: { page: 0, size: 20, sort: [] }
    },
    results: {
      total: 0, // total de registros
      startPage: 0,
      totalPage: 0, // total de la pagina
      hits: [],
      facets: [],
      quicksearch: []
    },
    // filtros componentes
    facets: [],
    facetsMobile: [],
    letters: [],
    orderBy: null,
    quicksearch: null,
    page: 1
    // hasta aqui
    // counter: 0,
    // checked: null,
    // quicksearchResults: []
    // resultados
  }
}

export { state as states }

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
