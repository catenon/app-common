// import { map } from 'lodash'
import { Users } from 'catenonLib/resources'

import { states } from './index'

export async function fetchResults ({ state, commit }, payload) {
  const fieldsFilter = state.filters.fieldsFilter
  const pageable = state.filters.pageable
  const query = state.filters.query
  const type = state.filters.type
  // si es mobile
  const isMobile = payload ? payload.isMobile : false
  // se ejecuta un filtro customizado o se cargan los globales
  const customFilters = payload ? payload.filters : null
  const filters = customFilters || { fieldsFilter, pageable, query, type }

  let data;
  if (type != 'client' && type != 'operation' && type != 'financialRecord') {
    // Por defecto, se usa el endpoint de professionals
    const response = await Users.searchProfessionals(filters)
    data = response.data

    commit('SET_RESULTS_FACETS', { facets: data.facets, isMobile })
    commit('SET_RESULTS_TOTAL', data.count)
    commit('SET_RESULTS_HITS', data.searchHits.professional.hits)
    commit('SET_RESULTS_START_PAGE', data.searchHits.professional.start)
    commit('SET_RESULTS_TOTAL_PAGE', data.searchHits.professional.total)
  }
};

export async function fetchQuicksearch ({ state, commit }, query) {
  const filters = { fieldsFilter: [], pageable: state.filters.pageable, query, type: 'professional' }
  const { data } = await Users.searchProfessionals(filters)
  commit('SET_RESULTS_QUICKSEARCH', data.searchHits.professional.hits)
}

export async function resetFilters ({ state }) {
  const reset = states()
  state.filters = reset.filters
  state.results = reset.results
  state.facets = []
  state.letters = []
  state.orderBy = null
  state.quicksearch = null
  state.page = 1
}
