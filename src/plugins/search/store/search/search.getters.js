// import { state as states } from './index'

// Filtros de los componentes
export const getOrderBy = state => state.orderBy
export const getLetters = state => state.letters
export const getFacets = state => state.facets
export const getFacetsMobile = state => state.facetsMobile
export const getChecked = state => state.checked
export const getQuicksearch = state => state.quicksearch
export const getFilters = state => state.filters
export const getPage = state => state.page

// export const getDefaultFilters = _ => states.filters
// Filtros de la búqueda
export const getFilterPage = state => state.filters.pageable.page
export const getFilterSize = state => state.filters.pageable.size
export const getFilterFields = state => state.filters.fieldsFilter
export const getFilterQuery = state => state.filters.query
export const getFilterSort = state => state.filters.pageable.sort

// Resultados de la búsqueda
export const getResultHits = state => state.results.hits
export const getResultFacets = state => state.results.facets
export const getResultTotal = state => state.results.total
export const getResultStartPage = state => state.results.startPage
export const getResultTotalPage = state => state.results.totalPage
export const getResultQuickseach = state => state.results.quicksearch
