// import Vue from 'vue'
import { clone, find, forEach, filter, sortBy, concat, includes, pull, findIndex, isEmpty } from 'lodash'

export const setLetters = (state, filter) => {
  if (filter === 'ALL') {
    state.letters = undefined
  } else {
    state.letters = { 'name': 'surnameFirstLetter', 'values': [filter] }
  }
}

export const SET_FACETS = (state, facets) => {
  state.facets = facets
}

export const setChecked = (state, filter) => {
  state.checked = filter
}

export const SET_QUICKSEARCH = (state, query) => {
  state.quicksearch = query
}

export const setPagination = (state, page) => {
  state.pagination = page
}

// export const setOrderBy = (state, filter) => {
//   state.orderBy = filter
// }

export const setFilters = (state, filters) => {
  state.filters = filters
}

export const setFilterPage = (state, page) => {
  state.filters.pageable.page = page
}

export const setFilterSize = (state, size) => {
  state.filters.pageable.size = size
}

export const setFilterType = (state, filterType) => {
  state.filters.type = filterType
}
// export const setFilterFields = (state, fieldsFilter) => {
//   state.filters.fieldsFilter = fieldsFilter
// }

/**
 *  Query
 * */
export const SET_FILTER_QUERY = (state, query) => {
  state.filters.query = query
}

/**
 *  Pageable
 * */
export const SET_FILTER_PAGE = (state, page) => {
  state.filters.pageable.page = page
  state.pagination = page
}

export const SET_FILTER_SIZE = (state, size) => {
  state.filters.pageable.size = size
}

export const SET_FILTER_SORT = (state, sort) => {
  state.filters.pageable.sort = sort
  // state.orderBy = filter
}

export const ADD_REMOVE_FILTER_SORT = (state, orderBy) => {
  const filters = clone(state.filters.pageable.sort)
  const filter = find(filters, { property: orderBy.property })
  const filterIndex = findIndex(filters, { property: orderBy.property })
  if (orderBy.direction) {
    // si tiene direction y existe el filtro, se actualiza, sino se añade
    if (filter) {
      filter.direction = orderBy.direction
    } else {
      filters.push({ property: orderBy.property, direction: orderBy.direction })
    }
  } else {
    // si no tiene direction y existe el filtro, se remueve
    if (filterIndex >= 0) {
      filters.splice(filterIndex, 1)
    }
  }
  state.filters.pageable.sort = filters
}

/**
 *  Field filters
 * */
export const SET_FILTER_LETTERS = (state, letter) => {
  const index = findIndex(state.filters.fieldsFilter, { name: 'surnameFirstLetter' })
  if (letter === 'ALL') {
    if (index >= 0) {
      state.filters.fieldsFilter.splice(index, 1)
    }
  } else {
    if (index >= 0) {
      state.filters.fieldsFilter[index].values = [letter]
    } else {
      state.filters.fieldsFilter.push({ 'name': 'surnameFirstLetter', 'values': [letter] })
    }
  }
}

export const SET_FILTER_FIELDS = (state, fieldsFilter) => {
  state.filters.fieldsFilter = fieldsFilter
}

export const ADD_REMOVE_FILTER_FACETS = (state, { facet, bucket }) => {
  const filters = clone(state.filters.fieldsFilter)
  const filter = find(filters, { name: facet.field })
  if (filter) {
    if (includes(filter.values, bucket.value)) {
      pull(filter.values, bucket.value)
    } else {
      filter.values.push(bucket.value)
    }
  } else {
    filters.push({
      name: facet.field,
      values: [bucket.value]
    })
  }
  // Elimina los filtros si el array de values esta vacío
  const index = findIndex(filters, filter => { return !!isEmpty(filter.values) })
  if (index >= 0) {
    filters.splice(index, 1)
  }
  state.filters.fieldsFilter = filters
}

export const RESET_FILTER_FACETS = (state) => {
  state.filters.fieldsFilter = [
    { name: 'validationStatus', values: ['Activated'] },
    { name: 'active', values: [true] }
  ]
}

/**
 *  Results
 * */
export const SET_RESULTS_FACETS = (state, payload) => {
  // console.log('EJECUTAR SET_RESULTS_FACETS')
  const facets = payload.facets
  // state.results.facets = sortBy(clone(facets), { primary: false })
  state.results.facets = clone(facets)
  // actualiza los filtros
  // (evaluar una opcion "replace=true|false" para trabajar sobre los filtros actuales o conservar los originales)
  forEach(facets, facet => {
    let hasSelectedFilter = false
    forEach(facet.buckets, bucket => {
      if (includes(facet.field, 'Code')) {
        bucket.code = `${facet.field}.${bucket.label}`
      }
      bucket.checked = false
      // valida si existe en los filtros
      const field = find(state.filters.fieldsFilter, { name: facet.field })
      if (field) {
        bucket.checked = includes(field.values, bucket.value)
      }
      // si se ha seleccionado un bucket se muestra desplegado
      if (bucket.checked) {
        hasSelectedFilter = true
      }
    })
    facet.collapsed = facet.primary || hasSelectedFilter
  })
  // console.log(payload.facets)
  // console.log(facets)
  const unCollapsed = filter(facets, { collapsed: true })
  const collapsed = filter(facets, { collapsed: false })
  // Vue.set(state.facets, concat(sortBy(unCollapsed, 'name'), sortBy(collapsed, 'name')))
  state.facets = concat(sortBy(unCollapsed, 'name'), sortBy(collapsed, 'name'))
  // if (!payload.isMobile) {
  state.facetsMobile = concat(sortBy(unCollapsed, 'name'), sortBy(collapsed, 'name'))
  // }
  // state.facets = concat(unCollapsed, collapsed)
}

export const SET_RESULTS_HITS = (state, hits) => {
  state.results.hits = hits
}

export const SET_RESULTS_TOTAL = (state, total) => {
  state.results.total = total
}

export const SET_RESULTS_START_PAGE = (state, start) => {
  state.results.start = start
}

export const SET_RESULTS_TOTAL_PAGE = (state, total) => {
  state.results.start = total
}

export const SET_RESULTS_QUICKSEARCH = (state, hits) => {
  state.results.quicksearch = hits
}
