import * as mutations from './auth.mutations'
import * as actions from './auth.actions'
import * as getters from './auth.getters'

export default {
  namespaced: true,
  state: {
    currentUser: {}
  },
  getters,
  mutations,
  actions
}
