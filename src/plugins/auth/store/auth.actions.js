import { isEmpty, includes } from 'lodash'
import util from '../../services/utils'

// export const login = ({ commit }, { vm }) => {
//   return new Promise((resolve, reject) => {
//     // console.log('Auth - dispatch login')
//     commit('SET_AUTH_STATUS', 'loading')
//     vm.$auth.loginWithRedirect()
//   })
// }

export const logout = async ({ commit }, { vm }) => {
  vm.$auth.logoutWithRedirect()
  // commit('SET_AUTH_STATUS', '')
  commit('SET_CURRENT_USER', {})
}

export const currentUser = ({ commit }, { currentUser }) => {
  // Set roles and permissions

  currentUser.id = currentUser._links.publicProfile ? util.getIdFromUrl(currentUser._links.publicProfile.href) : null

  // extra info
  currentUser.isAdmin = false

  // Roles del grupo Administration
  let isAdminRole = false

  // Roles del grupo User Management
  let isPMRole = false
  let isPVRole = false
  let isAARole = false

  currentUser.roles.forEach(role => {
    if (role === 'ADMINISTRATOR') {
      currentUser.isAdmin = isAdminRole = true
    } else if (role === 'PROFESSIONAL_MANAGER') {
      isPMRole = true
      // currentUser.isAdmin = isPMRole = true
    } else if (role === 'PROFESSIONAL_VALIDATOR') {
      isPVRole = true
    } else if (role === 'ACCOUNT_ACTIVATOR') {
      isAARole = true
    }
  })

  currentUser.isCompanyAdmin = includes(currentUser.roles, 'COMPANY_ADMINISTRATOR')
  currentUser.isCompanyManager = includes(currentUser.roles, 'COMPANY_MANAGER')

  // si no tiene roles solo puede visualizar información
  currentUser.hasViewPermission = isEmpty(currentUser.roles)
  // Si es admin tiene permisos de edición
  currentUser.hasEditPermission = (currentUser.isAdmin || isAdminRole || isPMRole)
  currentUser.hasContractPermission = (currentUser.isAdmin || isAdminRole || isPMRole || isPVRole)
  currentUser.hasRolesPermission = (isAdminRole || isAARole)
  currentUser.hasStatusPermission = (currentUser.isAdmin || isAdminRole || isAARole || isPVRole)

  currentUser.photo = (currentUser._links.profilePhotoUrl) ? currentUser._links.profilePhotoUrl.href : ''

  commit('SET_CURRENT_USER', currentUser)
}
