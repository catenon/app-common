export const SET_AUTH_STATUS = (state, value) => {
  state.authStatus = value
}

export const SET_ACCESS_TOKEN = (state, value) => {
  state.accessToken = value
}

export const SET_CURRENT_USER = (state, value) => {
  state.currentUser = value
}
