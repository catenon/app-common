import debug from 'debug'

import authRoute from './auth.route'
import authStore from './store'

const logger = debug('@catenon/auth:plugin')

const CatenonAuthPlugin = {
  install (Vue, { Auth, store, router }) {
    logger('Installing CatenonAuth plugin...')

    Vue.prototype.$auth = Auth
    Vue.prototype.$api = Auth.api
    // Vue.prototype.$axios = axios

    // Register Vuex auth module
    store.registerModule('auth', authStore)

    // Add auth routes
    router.addRoutes(authRoute)

    // // config router
    // configRouter(router, store)

    // // Inicializa la
    // initializeAuth(router, store)

    // CatenonAuth.auth = Auth.
  }
}

export default CatenonAuthPlugin
