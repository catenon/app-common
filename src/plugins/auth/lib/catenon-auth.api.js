import debug from 'debug'
import axios from 'axios'
// import { errorHandlerAPI } from '../../services/errorHandlers'

/** @ignore */
const logger = debug('@catenon/api')

class CatenonAPI {
  // USER = null

  constructor () {
    logger('CatenoneAPI constructor, configuring...')
    // if (window.catenon.api) {
    //   return window.catenon.api
    // }

    // axios.defaults.headers.common.authorization = `Bearer ${token}`
    this.axios = axios
    this.USER = createAPI(process.env.VUE_APP_API_USER)
    this.USER.interceptors.response.use(responseSuccessHandler, responseErrorHandler)
    this.CANDIDATE = createAPI(process.env.VUE_APP_API_CANDIDATE)
    this.CANDIDATE.interceptors.response.use(responseSuccessHandler, responseErrorHandler)
    this.CLIENT = createAPI(process.env.VUE_APP_API_CLIENT)
    this.CLIENT.interceptors.response.use(responseSuccessHandler, responseErrorHandler)
    logger('CatenoneAPI is ready!')
  }

  setAccessToken (token) {
    this.axios.defaults.headers.common.authorization = `Bearer ${token}`
    this.USER.defaults.headers.common.authorization = `Bearer ${token}`
    this.CANDIDATE.defaults.headers.common.authorization = `Bearer ${token}`
    this.CLIENT.defaults.headers.common.authorization = `Bearer ${token}`
  }
}

const createAPI = baseURL => {
  return axios.create({
    baseURL,
    timeout: 5000,
    headers: {
      'Accept': 'application/hal+json,application/json,text/plain,*/*'
    }
    // transformRequest: [function (data, headers) {
    //   console.log('transformRequest...')
    //   return data
    // }],
    // TODO se deben añadir al default para NO sobreescribir la respuesta
    // https://github.com/axios/axios/issues/430
    // transformResponse: axios.defaults.transformResponse.concat(data => {
    //   console.log('transformResponse typeof data...', typeof data)
    //   return data
    // })
  })
}

const responseSuccessHandler = response => {
  logger('Interceptor responseSuccessHandler (todo ok)')
  // console.dir(response)
  return response
}

const responseErrorHandler = error => {
  // if (isHandlerEnabled(error.config)) {
  //   // Handle errors
  // }
  // return Promise.reject({ ...error })
  // router.push('/error')
  // const errorHandler = error.config.errorHandler
  // console.dir(typeof errorHandler)
  // // if (errorHandler && typeof errorHandler === 'function') {
  // //   errorHandler.call()
  // // }
  // if (errorHandler && typeof errorHandler === 'object') {
  //   errorHandlerAPI(error.response.data, errorHandler.vm, errorHandler.text, errorHandler.title)
  // }

  logger('Interceptor responseErrorHandler: (por ahora no se hace nada)' + `${error.code} - ${error.message}`)
  // error handler
  // if (error.response) {
  //   // The request was made and the server responded with a status code
  //   // that falls out of the range of 2xx
  //   console.log(error.response.data)
  //   console.log(error.response.status)
  //   console.log(error.response.headers)
  // } else if (error.request) {
  //   // The request was made but no response was received
  //   // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
  //   // http.ClientRequest in node.js
  //   console.log(error.request)
  // } else {
  //   // Something happened in setting up the request that triggered an Error
  //   console.log('Error', error.message)
  // }
  // console.log(error.config)

  // BToast.toast(error.message, {
  //   title: error.code,
  //   autoHideDelay: 5000,
  //   solid: true,
  //   variant: 'danger'
  // })
  // alert(`${error.code} - ${error.message}`)

  // switch(error.response.status) {
  //   case 400:
  //     break
  //   case 401:
  //     break
  //   case 500:
  // axios.defaults.router.push('error')
  // }
  return Promise.reject(error)
}

// export { CatenonAPI }
export default new CatenonAPI()

// https://dev.to/teroauralinna/global-http-request-and-response-handling-with-the-axios-interceptor-30ae
