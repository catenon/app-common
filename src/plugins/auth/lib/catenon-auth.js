import assign from 'lodash/assign'
import defaultsDeep from 'lodash/defaultsDeep'
import get from 'lodash/get'
import set from 'lodash/set'
import uuid from 'uuid'
import debug from 'debug'

import Cookie from 'js-cookie'

// import { CatenonAuthProvider } from '../../../../_to_delete/catenon-auth.provider'
import { CatenonAuthProfile } from './catenon-auth.profile'
import { CatenonAuthUtilities } from './catenon-auth.utilities'
import api from './catenon-auth.api'

/** @ignore */
const logger = debug('@catenon/auth')

/**
 * Authentication Controller
 */
class CatenonAuth {
  /**
   * Sets up Salte Auth
   * @param {Config} config configuration for salte auth
   */
  constructor (config) {
    logger('CatenoneAuth constructor, configuring...')
    if (window.catenon.auth) {
      return window.catenon.auth
    }

    if (!config) {
      throw new ReferenceError('A config must be provided.')
    }

    /**
     * The registered listeners
     * @private
     */
    // this.$listeners = {}

    const protectedResources = process.env.VUE_APP_OAUTH_PROTECTED_RESOURCES.split(',')

    // console.log(window.location.origin, window.location.href)

    const encodeState = window.btoa(JSON.stringify({ origin: window.location.origin, href: window.location.href }))

    const defaultConfig = {
      provider: 'wso2',
      storageType: 'local',
      loginType: 'redirect',
      responseType: process.env.VUE_APP_OAUTH_RESPONSE_TYPE || 'token',
      providerUrl: config.site || process.env.VUE_APP_OAUTH_PROVIDER_URL,
      clientId: config.clientId || process.env.VUE_APP_OAUTH_CLIENT_ID,
      redirectUrl: config.redirectUrl || process.env.VUE_APP_OAUTH_REDIRECT_URL,
      scope: config.scope || process.env.VUE_APP_OAUTH_SCOPE,
      state: config.state || encodeState,
      endpoints: protectedResources,
      routes: false,
      validation: {
        nonce: false,
        aud: false,
        azp: false,
        state: false
      },
      queryParams: {
        state: encodeState
      },
      autoRefresh: false,
      autoRefreshBuffer: 60000,
      // Extra info
      profileUrl: config.profileUrl || '/me',
      redirectAfterLogin: config.redirectAfterLogin || '/'
    }

    /**
     * The API for Catenon Auth
     */
    // this.api = new CatenonAPI()

    /**
     * The configuration for salte auth
     * @type {Config}
     * @private
     */
    this.$config = config
    this.$config = defaultsDeep(config, defaultConfig)

    /**
     * Various utility functions for salte auth
     * @type {CatenonAuthUtilities}
     * @private
     */
    this.$utilities = new CatenonAuthUtilities(this.$config)

    /**
     * The user profile for salte auth
     * @type {CatenonAuthProfile}
     */
    this.profile = new CatenonAuthProfile(this.$config)

    this.profile.$parseParams()
    const error = this.profile.$validate()

    if (error) {
      console.error(error.code, ',', error.description)
      this.profile.$clear()
    }
    window.catenon.auth = this
    logger('CatenoneAuth is ready!')
  }

  // /**
  //  * Returns the configured provider
  //  * @type {Class|Object}
  //  * @private
  //  */
  // get $provider () {
  //   if (!this.$config.provider) {
  //     throw new ReferenceError('A provider must be specified')
  //   }
  //   if (typeof this.$config.provider === 'string') {
  //     const provider = CatenonAuthProvider
  //     if (!provider) {
  //       throw new ReferenceError(`Unknown Provider (${this.$config.provider})`)
  //     }
  //     return provider
  //   }
  //   return this.$config.provider
  // }

  /**
   * The authentication url to retrieve the access token
   * @type {String}
   * @private
   */
  get $accessTokenUrl () {
    logger('Access token url...')

    this.profile.$localState = window.btoa(JSON.stringify({ origin: window.location.origin, href: window.location.href }))
    this.profile.$nonce = uuid.v4()

    let authorizeEndpoint = `${this.$config.providerUrl}/oauth2/authorize`
    return this.$utilities.createUrl(authorizeEndpoint, assign({
      'state': this.$config.state, // this.profile.$localState,
      // 'nonce': this.profile.$nonce,
      'response_type': this.$config.responseType,
      'redirect_uri': (this.$config.redirectUrl && this.$config.redirectUrl.loginUrl) || this.$config.redirectUrl,
      'client_id': this.$config.clientId,
      'scope': this.$config.scope
      // 'prompt': 'none'
    }, this.$config.queryParams))
  }

  /**
   * The url to logout of the configured provider
   * @type {String}
   * @private
   */
  get $deauthorizeUrl () {
    // https://wso2-is.catenon.com/commonauth?commonAuthLogout=true&type=oauth2
    return this.$utilities.createUrl(`${this.$config.providerUrl}/commonauth`, {
      commonAuthLogout: true,
      type: 'oauth2'
      // commonAuthCallerPath: (this.$config.redirectUrl && this.$config.redirectUrl.logoutUrl) || this.$config.redirectUrl,
      // relyingParty: this.$config.relyingParty
    })
    // return this.$provider.deauthorizeUrl.call(this, defaultsDeep(this.$config, {
    //   idToken: this.profile.$idToken
    // }))
  }

  get user () {
    return this.profile.userInfo
  }

  get authenticated () {
    return !this.profile.accessTokenExpired
  }

  /**
   * Authenticates using the redirect-based OAuth flow.
   * @param {String} redirectUrl override for the redirect url, by default this will try to redirect the user back where they started.
   * @return {Promise} a promise intended to block future login attempts.
   *
   * @example
   * auth.loginWithRedirect() // Don't bother with utilizing the promise here, it never resolves.
   */
  loginWithRedirect (redirectUrl) {
    this.profile.$redirectUrl = redirectUrl
    // (redirectUrl && this.$utilities.resolveUrl(redirectUrl)) || this.profile.$redirectUrl || location.href
    this.profile.$actions(this.profile.$localState, 'login')
    this.$utilities.$navigate(this.$accessTokenUrl)
  }

  /**
   * Logs the user out of their configured identity provider.
   *
   * @example
   * auth.logoutWithRedirect()
   */
  logoutWithRedirect () {
    // const deauthorizeUrl = this.$deauthorizeUrl
    // https://wso2-is.catenon.com/commonauth?commonAuthLogout=true&type=oauth2
    logger('Logout with redirect...' + this.$deauthorizeUrl)
    Cookie.remove('catenonOAuthToken')
    this.profile.$clear()
    this.profile.$actions(this.profile.$localState, 'logout')
    logger('Generating logout iframe ')
    const iframe = document.createElement('iframe')
    iframe.style.display = 'none'
    iframe.src = this.$deauthorizeUrl
    document.body.appendChild(iframe)
    const $this = this
    setTimeout(function () {
      logger('Redirect after 3 seconds...')
      $this.loginWithRedirect($this.$config.redirectUrl)
    }, 3000)
    // loginWithRedirect()
    // AccessToken.destroy();
    // $rootScope.$broadcast('oauth:logging-out');
    // var path = params.site + '/commonauth?commonAuthLogout=true&type=oauth2';
    // const iframe = angular.element('<iframe></iframe>').attr('src', path).hide();
    // angular.element('body').append(iframe);
    // $rootScope.$broadcast('oauth:logout');
  }

  /**
   * Authenticates, requests the access token, and returns it if necessary.
   * @return {Promise<string>} a promise that resolves when we retrieve the access token
   */
  retrieveAccessToken () {
    return new Promise((resolve, reject) => {
      logger('Retrieve access token...')
      // console.log(this.profile)
      if (this.profile.accessTokenExpired) {
        logger('Access token not found or has expired, redirecting...' + this.$config.redirectUrl)
        this.loginWithRedirect(this.$config.redirectUrl)
        reject(new Error('Access Token not found'))
      } else {
        logger('Access token has found: ' + this.profile.$accessToken)
        // configurar las APIs
        api.setAccessToken(this.profile.$accessToken)
        // cargar los datos del usuario
        api.USER.get(this.$config.profileUrl)
          .then(results => {
            this.profile.userInfo = results.data
            resolve(this.profile.userInfo)
          })
          .catch(error => {
            reject(error)
          })
      }
    })
  }

  static get auth () {
    if (window.catenon.auth) {
      return window.catenon.auth
    }
    return null
  }

  static get api () {
    if (window.catenon.auth) {
      return window.catenon.api
    }
    return null
  }
}

set(window, 'catenon.CatenonAuth', get(window, 'catenon.CatenonAuth', CatenonAuth))
// export { CatenonAuth }
export default CatenonAuth
