import Redirecting from './views/Redirecting'
import Unauthorized from './views/Unauthorized'
export default [
  {
    path: '',
    name: 'redirecting',
    component: Redirecting,
    meta: {
      public: true
    }
  },
  {
    path: '/unauthorized',
    name: 'unauthorized',
    component: Unauthorized,
    meta: {
      public: true
    }
  },
  {
    path: '/logout',
    name: 'logout',
    component: Unauthorized,
    meta: {
      public: true
    }
  }
]
