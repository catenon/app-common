import Loading from './Loading.vue'
let vm = {}
export default {
  install (Vue) {
    const LoadingPlugin = Vue.extend(Loading)
    vm = new LoadingPlugin().$mount()
    document.body.appendChild(vm.$el)
    Vue.prototype.$loading = function (loading) {
      vm.loading = loading
    }
    Vue.prototype.$asyncLoading = function (fn) {
      return new Promise((resolve, reject) => {
        vm.loading = true
        const finished = cb => { return (result) => { cb(result); vm.loading = false } }
        fn.then(finished(resolve)).catch(finished(reject))
      })
    }
  }
}
