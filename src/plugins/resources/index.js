import Users from './users.resource'
import PublicProfiles from './publicProfile.resource'
import Languages from './languages.resource'
import Master from './master.resource'

export { Users, PublicProfiles, Languages, Master }

export default { Users, PublicProfiles, Languages, Master }
