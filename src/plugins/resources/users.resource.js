// import api from '../api'
import { CatenonAPI as api } from '../auth'

export default {
  searchProfessionals (filters) {
    // fieldsFilter, pageable, query, type
    // let url =
    // let data = { fieldsFilter, pageable, query, type }
    return api.USER.post('/professionals/search', filters)
  },
  getProfessional (id) {
    return api.USER.get(`/professionals/${id}`)
  }
}
