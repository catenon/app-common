import { CatenonAPI as api } from '../auth'
import axios from 'axios'

export default {

  getPublicProfile (id) {
    return api.USER.get(`/publicProfiles/${id}`)
  },
  loadSocialMediaProfiles (id) {
    return api.USER.get(`/publicProfiles/${id}/socialMediaProfiles`)
  },
  loadProfessionalExperiences (id) {
    return api.USER.get(`/publicProfiles/${id}/professionalExperiences`)
  },
  loadProfileEducation (id) {
    return api.USER.get(`/publicProfiles/${id}/educations`)
  },
  // loadResources () {
  //   const loadSectors = function () {
  //     return api.USER.get('/sectors')
  //   }
  //   const loadFunctionalAreas = function () {
  //     return api.USER.get('/functionalAreas')
  //   }
  //   return axios.all([loadSectors(), loadFunctionalAreas()])
  // },

  loadSectors () {
    return api.USER.get('/sectors')
  },
  loadFunctionalAreas () {
    return api.USER.get('/functionalAreas')
  },

  update (id, data) {
    return api.USER.put(`/publicProfiles/${id}`, data)
  },
  updateAvatar (id, data) {
    return api.USER.post(`/publicProfiles/${id}/photo`, data, {
      headers: {
        'Content-Type': undefined
      }
    })
  },
  // Profile details
  updateDetails (id, profile, data) {
    const updateProfile = function (id, data) {
      return api.USER.put(`/publicProfiles/${id}`, data)
    }
    const updateTwitter = function (twitter) {
      return api.USER.put(`/socialMediaProfiles/${twitter.id}`, twitter)
    }
    const updateFacebook = function (facebook) {
      return api.USER.put(`/socialMediaProfiles/${facebook.id}`, facebook)
    }
    const updateSkype = function (skype) {
      return api.USER.put(`/socialMediaProfiles/${skype.id}`, skype)
    }
    const updateLinkedin = function (linkedin) {
      return api.USER.put(`/socialMediaProfiles/${linkedin.id}`, linkedin)
    }
    return axios.all([updateProfile(id, profile), updateTwitter(data.twitter), updateFacebook(data.facebook), updateSkype(data.skype), updateLinkedin(data.linkedin)])
  },
  // Professional experiences
  updateExperience (experienceId, experience) {
    experience.area = experience.area ? experience.area : null
    experience.sector = experience.sector ? experience.sector : null
    return api.USER.put(`/professionalExperiences/${experienceId}`, experience)
  },
  addExperience (experience, id) {
    experience.area = experience.area ? experience.area : null
    experience.sector = experience.sector ? experience.sector : null
    return api.USER.post(`publicProfiles/${id}/professionalExperiences`, experience)
  },
  removeExperience (id) {
    return api.USER.delete(`/professionalExperiences/${id}`)
  },
  // Education
  updateEducation (educationId, education) {
    return api.USER.put(`/educations/${educationId}`, education)
  },
  addEducation (education, id) {
    return api.USER.post(`publicProfiles/${id}/educations`, education)
  },
  removeEducation (id) {
    return api.USER.delete(`/educations/${id}`)
  },
  // Language Skills
  loadLanguageSkills (id) {
    return api.USER.get(`/publicProfiles/${id}/languageSkills`)
  },
  createLanguageSkills (id, data) {
    return api.USER.post(`/publicProfiles/${id}/languageSkills`, data)
  },
  updateLanguageSkills (id, data) {
    return api.USER.put(`/languageSkills/${id}`, data)
  },
  deleteLanguageSkills (id) {
    return api.USER.delete(`/languageSkills/${id}`)
  }
}
