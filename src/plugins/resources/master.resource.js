import { CatenonAPI as api } from '../auth'

const Master = {

  getCountries () {
    return api.USER.get('/countries?page=0&size=250')
  },

  getLanguages () {
    return api.USER.get('/languages?page=0&size=250')
  },

  getLanguageSkillLevels () {
    return api.USER.get('/languageSkillLevels?page=0&size=250')
  }

}

export default Master
