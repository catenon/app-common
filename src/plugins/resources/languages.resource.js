import { CatenonAPI as api } from '../auth'

let Languages = {
  getLanguages () {
    return api.USER.get('/languages?page=0&size=250')
  },
  getLanguageSkillLevels () {
    return api.USER.get('/languageSkillLevels?page=0&size=250')
  }
}

export default Languages
