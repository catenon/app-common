export default [
  { path: '/', redirect: '/dashboard' },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: _ => import(/* webpackChunkName: "views" */ '@/views/Dashboard')
  },
  {
    path: '/panels',
    name: 'panels',
    component: _ => import(/* webpackChunkName: "views" */ '@/views/Panels')
  },
  {
    path: '/inputs',
    name: 'inputs',
    component: _ => import(/* webpackChunkName: "views" */ '@/views/Inputs')
  },
  {
    path: '/browse',
    name: 'browse',
    component: _ => import(/* webpackChunkName: "views" */ '@/views/Browse')
  },
  {
    path: '/professional/:id',
    name: 'professional',
    component: _ => import(/* webpackChunkName: "views" */ '@/views/Professional')
  },
  {
    path: '/helpers',
    name: 'helpers',
    component: _ => import(/* webpackChunkName: "views" */ '@/views/Helpers')
  },
  {
    path: '/loaders',
    name: 'loaders',
    component: _ => import(/* webpackChunkName: "views" */ '@/views/Loaders')
  },
  {
    path: '/icons',
    name: 'icons',
    component: _ => import(/* webpackChunkName: "views" */ '@/views/Icons')
  },
  {
    path: '/components',
    name: 'components',
    component: _ => import(/* webpackChunkName: "views" */ '@/views/Components')
  },
  {
    path: '/publicProfile/:id',
    name: 'publicProfile',
    component: _ => import(/* webpackChunkName: "views" */ '@/views/PublicProfile')
  }
]
