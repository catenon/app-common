# app-common

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


# Temas a explicar para los nuevos

- Lista de proyectos y resumen de cada uno de ellos
- App Common - detalles del proyecto
- Buenas practicas y convenciones de los proyectos (desarollar mas)